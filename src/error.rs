use std::error::Error as StdError;
use std::fmt;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Error {
    NotSupported,
    IncompatiblePathCharset,
    ObjcCallFailed,
    SystemCallFailed(Box<str>),
}

impl StdError for Error {
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match *self {
            Error::SystemCallFailed(ref desc) => desc,
            Error::IncompatiblePathCharset => "path can't be converted to UTF-8 C str",
            Error::ObjcCallFailed => "error while calling basic objc functions",
            Error::NotSupported => "excluding not supported on this platform",
        })
    }
}
