//! Marks a given path as excluded from backups.
//!
//! Currently implemented only for Time Machine on macOS.
//!
//! Applications that create caches and temporary files in non-standard system
//! locations should exclude these from backups to avoid unneccessary I/O churn
//! and backup bloat.

mod error;
#[cfg(target_os = "macos")]
mod macos;

use crate::error::Error;
use std::path::Path;

/// Marks given path as excluded from backups
///
/// On macOS excluded paths are marked using xattr, so if the file/dir is deleted,
/// the mark will be gone as well.
pub fn exclude_from_backups<P: AsRef<Path>>(_path: P) -> Result<(), Error> {
    #[cfg(target_os = "macos")]
    return macos::exclude_from_backups(_path);

    #[cfg(not(target_os = "macos"))]
    Err(Error::NotSupported)
}
