use exclude_from_backups as efb;
use std::env;
use std::path::Path;
use std::process;

fn main() {
    for arg in env::args_os().skip(1) {
        let path = Path::new(&arg);
        if let Err(err) = efb::exclude_from_backups(path) {
            eprintln!("{}: {}", path.display(), err);
            process::exit(1);
        }
    }
}
