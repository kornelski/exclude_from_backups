# Rust library for backup-invisible files and directories

Marks a given path as excluded from backups. Currently implemented only for Time Machine on macOS.

Applications that create caches and temporary files in non-standard system
locations should exclude these from backups to avoid unneccessary I/O churn
and backup bloat.

## Usage

It's available as a Rust library:

```rust
extern crate exclude_from_backups as efb;

efb::exclude_from_backups(&path)?;
```

or a CLI command:

```sh
cargo install exclude_from_backups
exclude_from_backups ~/*/target/release
```
